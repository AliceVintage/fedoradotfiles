This is a collection of my personal dotfiles for Linux. These dotfiles are designed to work on Fedora, but can easily be adapted to other Linux distributions.

**Installation**

To install the dotfiles, clone the repository to your home directory:

git clone https://gitlab.com/AliceVintage/fedoradotfiles/ ~/fedoradotfiles

Then, run the ansible playbook:

`sudo ansible-playbook -f 10 ~/fedoradotfiles/aquarius_playbook.yml`

After, stow everything inside the fedoradotfiles folder

`cd ~/fedoradotfiles && stow */`


This will create symlinks for all of the dotfiles in the repository in your home directory.

Feel free to fork this repository and make it your own. If you have any suggestions or improvements, please open a pull request.
License

This project is licensed under the MIT License.
